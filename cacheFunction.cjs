function cacheFunction(cb) {

    if (arguments.length !== 1 || typeof (cb) !== 'function') {
        throw new Error('The callback function is undefined or incomplete');
    }

    const cache = {};

    return function (...args) {
        const key = args.join('|');
        if (cache[key]) {
            console.log('Returning cached result');
            return cache[key];
        }
        console.log('Calling function');
        const result = cb.apply(this, args);
        cache[key] = result;
        return result;
    };
}

module.exports = cacheFunction;

