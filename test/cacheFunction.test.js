const cacheFunction = require('../cacheFunction.cjs');

test('cacheFunction test', () => {
  try {
    const square = cacheFunction((x) => {
      console.log('Calculating square');
      return x * x;
    });
    expect(square(5)).toBe(25);
    expect(square(5)).toBe(25);
    expect(square(6)).toBe(36);
    expect(square(6)).toBe(36);
  } catch (err) {
    console.error(err);
  }
});

