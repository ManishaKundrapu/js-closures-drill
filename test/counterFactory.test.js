const counterFactory = require('../counterFactory.cjs');
const assert = require('assert');

test('Testing Counter Factory', () => {
  const counter = counterFactory();
  assert.strictEqual(counter.increment(), 1);
  assert.strictEqual(counter.increment(), 2);
  assert.strictEqual(counter.increment(), 3);
  assert.strictEqual(counter.decrement(), 2);
  assert.strictEqual(counter.decrement(), 1);
});




