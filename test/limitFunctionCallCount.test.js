const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

function greet(name) {
    return ('Hi, ' + name + '!');
}

const Greet = limitFunctionCallCount(greet, 3);

test('Testing limitFunctionCallCount function', () => {
    try {
        expect(Greet('Anna')).toStrictEqual('Hi, Anna!');
        expect(Greet('Elsa')).toStrictEqual('Hi, Elsa!');
        expect(Greet('Olaf')).toStrictEqual('Hi, Olaf!');
        expect(Greet('Sven')).toStrictEqual(null);
    } catch (error) {
        console.error(err);

    }
});

module.exports = limitFunctionCallCount;

